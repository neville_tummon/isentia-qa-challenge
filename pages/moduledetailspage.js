var xpaths = require('./xpaths.js');

var ModulePage = function(linkElement) {
    this.navigate = function() {
        linkElement.click();
    }

    this.getTitle = function() {
        var title = element(by.xpath(xpaths.MODULE_TITLE_DETAILS_PAGE));
        return title.getText();
    }
}

module.exports = ModulePage;