var xpaths = require('./xpaths.js');
var ModulePage = require('./moduledetailspage.js');

var MediaPortalPage = function() {
    this.navigate = function() {
        browser.waitForAngularEnabled(false);
        browser.get('http://www.isentia.com/tools/mediaportal');
    }

    this.getModuleCount = function() {
        var elements = element.all(by.xpath(xpaths.ALL_MODULES_ON_MEDIA_PAGE));
        return elements.count();
    }

    this.getModuleTitle = function(index) {
        var elements = element.all(by.xpath(xpaths.ALL_MODULES_ON_MEDIA_PAGE));
        var targetElement = elements.get(index).element(by.css("strong"));
        return targetElement.getText();
    }

    this.getModuleDetailPage = function(index) {
        var elements = element.all(by.xpath(xpaths.ALL_MODULES_ON_MEDIA_PAGE));
        var linkElement = elements.get(index).element(by.css("a"));
        return new ModulePage(linkElement);
    }
}

module.exports = new MediaPortalPage();

