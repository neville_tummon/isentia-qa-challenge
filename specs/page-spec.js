var MediaPortalPage = require('../pages/mediaportalpage.js');

describe('Isentia QA challenge page', function() {
  it ('should show correct number of modules', function() {
    MediaPortalPage.navigate();
    expect(MediaPortalPage.getModuleCount()).toEqual(3);
  });

  it('should show correct header of each module on page', function() {
   MediaPortalPage.navigate();
   expect(MediaPortalPage.getModuleTitle(0)).toEqual("Connect");
   expect(MediaPortalPage.getModuleTitle(1)).toEqual("News and Analytics");
   expect(MediaPortalPage.getModuleTitle(2)).toEqual("Social");
  });

  it('should open correct module page', function() {
    MediaPortalPage.navigate();
    var DetailPage1 = MediaPortalPage.getModuleDetailPage(0);
    DetailPage1.navigate();
    expect(DetailPage1.getTitle()).toContain("Connect");

    MediaPortalPage.navigate();
    var DetailPage2 = MediaPortalPage.getModuleDetailPage(1);
    DetailPage2.navigate();
    expect(DetailPage2.getTitle()).toContain("News & Analytics");

    MediaPortalPage.navigate();
    var DetailPage3 = MediaPortalPage.getModuleDetailPage(2);
    DetailPage3.navigate();
    expect(DetailPage3.getTitle()).toContain("Social");
  });
});