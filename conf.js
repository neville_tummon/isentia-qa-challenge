exports.config = {
  seleniumAddress: process.env.SELENIUM_ADDRESS || 'http://localhost:4444/wd/hub',
  specs: ['specs/page-spec.js'],
  capabilities: {
    'browserstack.user': process.env.BROWSERSTACK_USERNAME || 'USERNAME',
    'browserstack.key': process.env.BROWSERSTACK_ACCESS_KEY || 'ACCESS_KEY',
    os: process.env.PROTRACTOR_OS || 'Windows',
    os_version: process.env.PROTRACTOR_OS_VERSION || '8',
    browserName: process.env.PROTRACTOR_BROWSER || 'IE',
    browser_version: process.env.PROTRACTOR_BROWSER_VERSION || '10.0',
    resolution: '1024x768',
  }
};